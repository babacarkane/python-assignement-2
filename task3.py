import numbers
from tokenize import String


INITIAL_AMOUNT = 50000


def printAmount(name: String, amount: numbers):
    print(name + ": ", amount, "GHS")


print("------------------------------------------")
printAmount("Initial Amount", INITIAL_AMOUNT)
print("------------------------------------------")
printAmount("Marketing", int(INITIAL_AMOUNT * 0.25))
printAmount("Operation", int(INITIAL_AMOUNT * 0.5))
printAmount("Customer acquisition", int(INITIAL_AMOUNT * 0.25))
print("------------------------------------------")
print("Number of customers: ", int((INITIAL_AMOUNT * 0.25) / 5))
print("------------------------------------------")
